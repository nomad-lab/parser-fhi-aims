# List of Example Files
This directory contains examples calculations of FHI-aims.
The content of the folders and files is detailed below.

## Folders
### Fe_band_structure_dos_spin
Band structure, DOS, and stom projected DOS calculation of bcc Fe with pw-lda, atomic_zora and collinear spin.

### NaCl_dos
DOS, and species projected DOS calculation of NaCl crystal with pw-lda.

### no_verbatim_writeout
This folder contains a calculation where the contents of control.in and
geometry.in are not repeated verbatim in the output ouf FHI-aims.

### Si_band_structure
Band structure calculation of diamond Si with pw-lda.

## Single Files
### Au2_non-periodic_geometry_optimization.out
Geometry optimization of Au~2~ molecule with pbe and atomic_zora.

### Au2_non-periodic_scalar_ZORA.out
Total energy calculation of Au~2~ molecule with pbe and scalar ZORA.

### Fe_periodic_spin.out
Total energy calculation of bcc Re with pw-lda, atomic_zora and collinear spin.
Eigenvalues are written on 4 k-points per spin channel.

### H2O_periodic_MD.out
Molecular dynamics simulation of H~2~0 molecule with pw-lda in a unit cell.

### N_non-periodic_spin.out
Total energy calculation of N atom with hse06 and collinear spin.

### Si_periodic_geometry_optimization.out
Unit cell optimization of diamond Si with pw-lda.
The fractional coordinates of the atoms are fixed.

