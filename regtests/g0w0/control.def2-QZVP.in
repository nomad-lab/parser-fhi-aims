################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2007
#
#  Suggested "safe" defaults for C atom (to be pasted into control.in file)
#
################################################################################
  species        C
   include_min_basis   false         
   pure_gauss          true          
   cut_pot             6.0  2.5  1.0 
   l_hartree           8             
   basis_dep_cutoff    0.d0          
   radial_base         100 7.0       
   radial_multiplier   8             
   angular_grids auto                
   angular             1202          
   angular_acc         1.0e-08       
   angular_min         110           
   basis_acc           1.0e-5        
#     global species definitions
    nucleus             6
    mass                12.0107
#
#
#
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   2.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   1.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.0 A, 1.25 A, 1.5 A, 2.0 A, 3.0 A
#
################################################################################
#  "First tier" - improvements: -1214.57 meV to -155.61 meV
#  "Second tier" - improvements: -67.75 meV to -5.23 meV
#  "Third tier" - improvements: -2.43 meV to -0.60 meV
#  "Fourth tier" - improvements: -0.39 meV to -0.18 meV
#  Further basis functions - improvements: -0.08 meV and below
# C def2-QZVP
 gaussian 0 8
     67025.0710290            0.0000387  
     10039.9865380            0.0003011  
      2284.9316911            0.0015788  
       647.1412213            0.0066087  
       211.0947234            0.0233671  
        76.1776439            0.0704207  
        29.6338392            0.1736034  
        12.1877851            0.3229231  
 gaussian 0 2
        53.0260063            0.0748974  
        15.2585028            0.7613622  
 gaussian 0 1 5.2403957464
 gaussian 0 1 2.2905022379
 gaussian 0 1 0.69673283006
 gaussian 0 1 0.27599337363
 gaussian 0 1 0.10739884389
 gaussian 1 5
       105.1255508            0.0008465  
        24.8844611            0.0066274  
         7.8637231            0.0301204  
         2.8407002            0.0999514  
         1.1227137            0.2382630  
 gaussian 1 1 0.46050725555
 gaussian 1 1 0.18937530913
  gaussian           1          1   0.075984 
 gaussian 2 1 1.84800000
 gaussian 2 1 0.64900000
 gaussian 2 1 0.22800000
 gaussian 3 1 1.41900000
 gaussian 3 1 0.48500000
 gaussian 4 1 1.01100000
################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2007
#
#  Suggested "safe" defaults for O atom (to be pasted into control.in file)
#
################################################################################
  species        O
   include_min_basis   false         
   pure_gauss          true          
   cut_pot             6.0  2.5  1.0 
   l_hartree           8             
   basis_dep_cutoff    0.d0          
   radial_base         100 7.0       
   radial_multiplier   8             
   angular_grids auto                
   angular             1202          
   angular_acc         1.0e-08       
   angular_min         110           
   basis_acc           1.0e-5        
#     global species definitions
    nucleus             8
    mass                15.9994
#
#
#
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   4.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   3.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.0 A, 1.208 A, 1.5 A, 2.0 A, 3.0 A
#
################################################################################
#  "First tier" - improvements: -699.05 meV to -159.38 meV
#  "Second tier" - improvements: -49.91 meV to -5.39 meV
#  "Third tier" - improvements: -2.83 meV to -0.50 meV
#  "Fourth tier" - improvements: -0.40 meV to -0.12 meV
# Further basis functions - -0.08 meV and below
# O def2-QZVP
 gaussian 0 8
    116506.4690800            0.0000404  
     17504.3497240            0.0003126  
      3993.4513230            0.0016341  
      1133.0063186            0.0068283  
       369.9956959            0.0241244  
       133.6207435            0.0727302  
        52.0356436            0.1793443  
        21.4619393            0.3305959  
 gaussian 0 2
        89.8350513            0.0964687  
        26.4280108            0.9411748  
 gaussian 0 1 9.2822824649
 gaussian 0 1 4.0947728533
 gaussian 0 1 1.3255349078
 gaussian 0 1 0.51877230787
 gaussian 0 1 0.19772676454
 gaussian 1 5
       191.1525581            0.0025116  
        45.2333567            0.0200392  
        14.3534659            0.0936091  
         5.2422372            0.3061813  
         2.0792419            0.6781050  
 gaussian 1 1 0.84282371424
 gaussian 1 1 0.33617694891
 gaussian 1 1 0.12863997974
 gaussian 2 1 3.77500000
 gaussian 2 1 1.30000000
 gaussian 2 1 0.44400000
 gaussian 3 1 2.66600000
 gaussian 3 1 0.85900000
 gaussian 4 1 1.84600000
################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2007
#
#  Suggested "safe" defaults for H atom (to be pasted into control.in file)
#
################################################################################
  species        H
   include_min_basis   false         
   pure_gauss          true          
   cut_pot             6.0  2.5  1.0 
   l_hartree           8             
   basis_dep_cutoff    0.d0          
   radial_base         100 7.0       
   radial_multiplier   8             
   angular_grids auto                
   angular             1202          
   angular_acc         1.0e-08       
   angular_min         110           
   basis_acc           1.0e-5        
#     global species definitions
    nucleus             1
    mass                1.00794
#
#
#     
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      1  s   1.
#     ion occupancy
    ion_occ      1  s   0.5
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Basis constructed for dimers: 0.5 A, 0.7 A, 1.0 A, 1.5 A, 2.5 A
#
################################################################################
#  "First tier" - improvements: -1014.90 meV to -62.69 meV
#  "Second tier" - improvements: -12.89 meV to -1.83 meV
#  "Third tier" - improvements: -0.25 meV to -0.12 meV
# H def2-QZVP
 gaussian 0 4
       190.6916900            0.0007082  
        28.6055320            0.0054679  
         6.5095943            0.0279666  
         1.8412455            0.1076454  
 gaussian 0 1 0.59853725
 gaussian 0 1 0.21397624
  gaussian           0          1   0.080316 
 gaussian 1 1 2.29200000
 gaussian 1 1 0.83800000
 gaussian 1 1 0.29200000
 gaussian 2 1 2.06200000
 gaussian 2 1 0.66200000
 gaussian 3 1 1.39700000
